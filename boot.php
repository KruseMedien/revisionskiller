<?php
/*
 * Copyright (c) 2015, KruseMedien GmbH, http://www.krusemedien.com
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */
define('KM24_PATH', __DIR__.'/');
$container['sly-classloader']->add('', __DIR__.'/lib');
$container['sly-i18n']->appendFile(__DIR__.'/lang');

$container['sly-revisionskiller-service'] = $container->share(function($container) {
	$config   = $container['sly-service-addon']->getProperty('krusemedien/revisionskiller', '', array());

	return new KM24\Service($config);
});
$container['sly-revisionskiller-listeners'] = $container->share(function($container) {
	return new KM24\Listeners();
});

// Register Events
$dispatcher = sly_Core::dispatcher();
if (sly_Core::isBackend()) {
	$dispatcher->addListener('SLY_BACKEND_NAVIGATION_INIT',     array('%sly-revisionskiller-listeners%', 'backendNavigation'));
}