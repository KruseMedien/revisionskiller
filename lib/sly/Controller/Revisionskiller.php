<?php
/*
 * Copyright (c) 2015, KruseMedien GmbH, http://www.krusemedien.com
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */
use sly\Assets\Util;
use KM24\Killer;
use KM24\TimeParser;
class sly_Controller_Revisionskiller extends sly_Controller_Backend implements sly_Controller_Interface {
	public function indexAction() {
		$name      = 'krusemedien/revisionskiller';
		$container = $this->getContainer();
		$layout    = $container['sly-layout'];
		$service   = $container['sly-service-addon'];
		$config    = $container['sly-revisionskiller-service']->getConfig(null);
		$version   = $service->getPackageService()->getVersion($name);

		$layout->pageHeader(t('km24_title'));

		print sly_Helper_Message::renderFlashMessage($container['sly-flash-message']);

		$this->render('index.phtml', array(
			'config'  => $config
		), false);
	}

	public function updateAction() {
		sly_Util_Csrf::checkToken();

		$request  = $this->getRequest();
		$delay     = TimeParser::decodeString($request->post('delay', 'string'));
		$limit     = $request->post('limit', 'string');

		$container   = $this->getContainer();
		$service     = $container['sly-service-addon'];
		$name        = 'krusemedien/revisionskiller';

		$service->setProperty($name, 'delay',    $delay);
		$service->setProperty($name, 'limit',    $limit);

		$container['sly-flash-message']->appendInfo(t('km24_config_saved'));

		return $this->redirectResponse();
	}

	public function processAction() {
		Killer::process();
		$container   = $this->getContainer();
		$container['sly-flash-message']->appendInfo(t('km24_revs_killed'));
		return $this->redirectResponse();
	}


	public function checkPermission($action) {
		$user = $this->getContainer()->get('sly-service-user')->getCurrentUser();

		return $user && ($user->isAdmin() || $user->hasRight('pages', 'revisionskiller'));
	}

	protected function getViewFolder() {
		return KM24_PATH.'/templates/';
	}
}
