<?php
/*
 * Copyright (c) 2015, KruseMedien GmbH, http://www.krusemedien.com
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */
namespace KM24;
use sly_Util_Article;
use WV8_Settings;
use sly_Core;
use sly_Util_YAML;
use sly_Service_Article;
class Killer {
	
	public static function killRevs($id){

		$name      = 'krusemedien/revisionskiller';
		$container = sly_Core::getContainer();
		$config    = $container['sly-revisionskiller-service']->getConfig(null);

		set_time_limit(0);
		$container = sly_Core::getContainer();
		$current = time();
		if($config['delay']){
			$delay = $config['delay'];
		}else{
			$delay = 864000; // 10 Tage 
		}
		// sly_dump($delay);
		// die;
		$limit = $current - $delay;
		
		if($config['limit']){
			$revLimit = $config['limit'];
		}else{
			$revLimit = 10;
		}

		$art = sly_Util_Article::findById($id, 1);
		if($art){
			// Artikelservice starten
			$articleService = new sly_Service_Article();
			// Revisionen des aktuell behandelten Artikel bekommen
			$articleRevs = $articleService->findAllRevisions($art->getId(),1, null, null, 'ASC');
			$articleRevsCount = count($articleService->findAllRevisions($art->getId(),1, null, null, 'ASC'));
			foreach($articleRevs as $articleRev){
				

				// zu löschende Artikel, basierend auf der Revisionsnummer
			 	$deleteArticle = sly_Util_Article::findById($art->getId(), 1, $articleRev->getRevision());

				// 0te Rev belassen (Kategoriebug)
				if($articleRev->getRevision() < 1) continue;
				
			 	// Gibt es weniger als 10 Revision? Continue
				if($articleRevsCount <= $revLimit) continue;
				
				// Aktuell Rev online? Break = neuere Revisionen nicht löschen
				if($deleteArticle->isOnline()) break;

				// ist die Revision älter als das Limit? Löschen!
				if($deleteArticle->getUpdatedate() < $limit){	
					$container->getArticleService()->purgeArticleRevision($deleteArticle);
					$articleRevsCount--;
				}
			}
		}
	}

	public static function process(){
		$types = self::readTypes();
		if($types){
			foreach($types as $key => $type){
				$arts = sly_Util_Article::findByType($key, true, 1);
				foreach($arts as $art){
					if(!$art) continue;
					self::killRevs($art->getId());
				}

			}
		}
	}

	protected static function readTypes(){
		$filename = SLY_DEVELOPFOLDER.'/config/articletypes.yml';
		if (!file_exists($filename)) return;
		$config = sly_Util_YAML::load($filename);
		return $config['article_types'];
	}	

}
?>