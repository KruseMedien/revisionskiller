<?php
/*
 * Copyright (c) 2015, KruseMedien GmbH, http://www.krusemedien.com
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */
namespace KM24;
use sly_Util_String;
class TimeParser {
	public static function decodeString($str) {
		static $factors = array('s' => 1, 'm' => 60, 'h' => 3600, 'd' => 86400, 'w' => 604800);

		if (empty($str)) return 0;
		if (sly_Util_String::isInteger($str)) return (int) $str;

		$sum = 0;

		preg_match_all('#(?:(\d+) ?([wdhms]))#i', $str, $matches, PREG_SET_ORDER);

		foreach ($matches as $match) {
			$sum += $match[1] * $factors[$match[2]];
		}
		return $sum;
	}

	public static function prettyPrint($timestamp) {
		$weeks      = floor($timestamp / ($week = 3600*24*7));
		$timestamp -= $weeks * $week;

		$days       = floor($timestamp / ($day = 3600*24));
		$timestamp -= $days * $day;

		$hours      = floor($timestamp / 3600);
		$timestamp -= $hours * 3600;

		$minutes = floor($timestamp / 60);
		$seconds = $timestamp - ($minutes * 60);
		$result  = array();

		foreach (array('weeks', 'days', 'hours', 'minutes', 'seconds') as $var) {
			$val  = $$var;
			$unit = $var[0]; // w, d, h, m, s

			if ($val > 0) $result[] = $val.$unit;
		}

		return implode(' ', $result);
	}
}