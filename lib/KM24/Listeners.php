<?php
/*
 * Copyright (c) 2015, KruseMedien GmbH, http://www.krusemedien.com
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */
namespace KM24;

use sly\Assets\App;
use sly_Layout_Navigation_Backend;
/**
 * @author Robert
 */
class Listeners implements \sly_ContainerAwareInterface {
	protected $container;

	public function setContainer(\sly_Container $container = null) {
		$this->container = $container;
	}

	/**
	 * SLY_BACKEND_NAVIGATION_INIT
	 */
	public function backendNavigation($nav) {
		$user = $this->container['sly-service-user']->getCurrentUser();
		if ($user !== null && ($user->isAdmin() || $user->hasPermission('pages', 'revisionskiller'))) {
			$group = $nav->addPage('addon','revisionskiller',t('km24_title'));
		}
	}

}
