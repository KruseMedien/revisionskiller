<?php
/*
 * Copyright (c) 2015, KruseMedien GmbH, http://www.krusemedien.com
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */
namespace KM24;

use Gaufrette\Adapter\Local;
use sly_Filesystem_Filesystem;
use sly_Filesystem_Service;


class Service implements \sly_ContainerAwareInterface {
	protected $container;
	protected $cacheDir;
	protected $config;

	public function __construct(array $config) {
		$this->config   = $config;
	}

	public function setContainer(\sly_Container $container = null) {
		$this->container = $container;
	}

	public function getCacheDir() {
		return $this->cacheDir;
	}

	/**
	 * get a config property of the addon
	 *
	 * @param  string $key      the config key
	 * @param  mixed  $default  a default value if the entry not exists
	 * @return mixed            the config value
	 */
	public function getConfig($key, $default = null) {
		if ($key === null) return $this->config;

		return isset($this->config[$key]) ? $this->config[$key] : $default;
	}

	public function getControlFile($realFile) {
		return $this->cacheDir.DIRECTORY_SEPARATOR.'control_'.sha1($realFile).'.json';
	}

}
